terraform {
    backend "gcs" {
        bucket = "config-management-tfstate"
        credentials = "creds/config-mgmt.json"
    }
}