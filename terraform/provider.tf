provider "google" {
  credentials = "${file("creds/config-mgmt.json")}"
  project     = "config-management"
  region      = "us-east1-b"
}
