resource "google_container_cluster" "gke-cluster" {
  name               = "terraform-jenkins-poc"
  network            = "default"
  zone               = "us-east1-b"
  initial_node_count = 3
}
